<!-- simple script to remove all tags from an xml file directory and print them into the dom -->

<!-- use: To help gain word counts from xml -->

<!-- instructions for use -->
<!-- 1. add this to run on a local apache install -->
<!-- 2. make a folder named xml next to this file -->
<!-- 3. add your xml files to the folder   -->
<!-- 4. run script, see output -->
<!-- 5. copy and paste text into word, get the word count job done :) -->

<?php

    $dir    = 'xml'; // dir
    $files1 = scandir($dir); //scan xml dir
    $array = array_values($files1); //scan dir for files
    $xmlFileLength = count($files1); //length of files inside dir
    $xml;
    
    for ($x=0; $x<=$xmlFileLength; $x++){
        
        $currXmlFile = 'xml/' . $array[$x]; // each xml file
        $xml = file_get_contents($currXmlFile); // get contents of xml file
        $xml = nl2br($xml); // remove tags 
        echo strip_tags($xml);

        
    }
    
?>
